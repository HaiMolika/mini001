package com.kshrd.mini1.repositories;

import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;

import com.kshrd.mini1.data.models.Article;
import com.kshrd.mini1.data.models.ArticlePost;
import com.kshrd.mini1.data.models.ArticleResponse;
import com.kshrd.mini1.data.models.ImageResponse;
import com.kshrd.mini1.data.models.Pagination;
import com.kshrd.mini1.data.remote.APIService;
import com.kshrd.mini1.data.remote.RetrofitInstance;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleRepository {

    private static final String TAG = "ArticleRepository tag";

    private APIService apiService;
    private MutableLiveData<ArticleResponse> articleResponse;
    private MutableLiveData<ArticlePost> updateResponse;
    private MutableLiveData<ImageResponse> imageResponse;
    private MutableLiveData<ArticlePost> articlePost;

    public ArticleRepository(){
        articleResponse = new MutableLiveData<>();
        apiService = RetrofitInstance.createService(APIService.class);
    }

    public MutableLiveData<ArticleResponse> getArticleByPagination(Pagination pagination){
        Call<ArticleResponse> call = apiService.getArticles(pagination.getPage(), pagination.getLimit());
        call.enqueue(new Callback<ArticleResponse>() {
            @Override
            public void onResponse(Call<ArticleResponse> call, Response<ArticleResponse> response) {
                if (response.isSuccessful()){
                    articleResponse.setValue(response.body());
                }
            }
            @Override
            public void onFailure(Call<ArticleResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
        return articleResponse;
    }

    public MutableLiveData<ImageResponse> postImage(File file){
        imageResponse = new MutableLiveData<>();
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getAbsolutePath(), requestBody);
        Call<ImageResponse> call = apiService.uploadImage(body);
        call.enqueue(new Callback<ImageResponse>() {
            @Override
            public void onResponse(Call<ImageResponse> call, Response<ImageResponse> response) {
                if (response.isSuccessful()){
                    imageResponse.setValue(response.body());
                }
                Log.d( "image " , response.message());
            }

            @Override
            public void onFailure(Call<ImageResponse> call, Throwable t) {
                Log.d( "image: " , t.getMessage());
            }
        });

        return imageResponse;
    }

    public MutableLiveData<ArticlePost> postArticle(ArticlePost post){

        articlePost = new MutableLiveData<>();
        Call<ArticlePost> call = apiService.postArticle(post);
        call.enqueue(new Callback<ArticlePost>() {
            @Override
            public void onResponse(Call<ArticlePost> call, Response<ArticlePost> response) {
                if (response.isSuccessful()){
                    articlePost.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ArticlePost> call, Throwable t) {
                Log.d("repository tag", "onFailure: " + t.getMessage());
            }
        });
        return articlePost;
    }

    public void deleteArticle(int id){

        Log.d( "delete", "Repository");
        Call<Article> call = apiService.deleteArticle(id);
        call.enqueue(new Callback<Article>() {
            @Override
            public void onResponse(Call<Article> call, Response<Article> response) {
                Log.d(TAG, "onResponse: Delete succesfully");
            }

            @Override
            public void onFailure(Call<Article> call, Throwable t) {
                Log.d("delete", "onFailure: " + t.getMessage());
            }
        });

    }


    public MutableLiveData<ArticlePost> updateArticle(int id, ArticlePost articlePost){
        updateResponse = new MutableLiveData<>();
        Call<ArticlePost> call = apiService.updateArticle(id, articlePost);
        call.enqueue(new Callback<ArticlePost>() {
            @Override
            public void onResponse(Call<ArticlePost> call, Response<ArticlePost> response) {
                if (response.isSuccessful()){
                    updateResponse.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ArticlePost> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });

        return updateResponse;
    }

}
