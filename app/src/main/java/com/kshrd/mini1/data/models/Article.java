package com.kshrd.mini1.data.models;

public class Article {

    private int id;
    private String title;
    private String description;
    private String created_date;
    private String image_url;

    public Article() {
    }

    public Article(int id, String titile, String description, String created_date, String image_url) {
        this.id = id;
        this.title = titile;
        this.description = description;
        this.created_date = created_date;
        this.image_url = image_url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    @Override
    public String toString() {
        return "Articles{" +
                "id=" + id +
                ", titile='" + title + '\'' +
                ", description='" + description + '\'' +
                ", created_date='" + created_date + '\'' +
                ", image_url='" + image_url + '\'' +
                '}';
    }
}
