package com.kshrd.mini1.data.models;

import com.google.gson.annotations.SerializedName;

public class ImageResponse {

    @SerializedName("code")
    private int code;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private String data;

    public ImageResponse(int code, String message, String data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ImageResponse() {
    }

    @Override
    public String toString() {
        return "ImageResponse{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data='" + data + '\'' +
                '}';
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
