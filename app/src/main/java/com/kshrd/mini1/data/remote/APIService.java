package com.kshrd.mini1.data.remote;

import com.kshrd.mini1.data.models.Article;
import com.kshrd.mini1.data.models.ArticlePost;
import com.kshrd.mini1.data.models.ArticleResponse;
import com.kshrd.mini1.data.models.ImageResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIService {

    @GET("v1/api/articles")
    Call<ArticleResponse> getArticles(@Query("page") int page, @Query("limit") int limit);

    @Multipart
    @POST("/v1/api/uploadfile/single")
    Call<ImageResponse> uploadImage(@Part MultipartBody.Part part);

    @POST("v1/api/articles/")
    Call<ArticlePost> postArticle(@Body ArticlePost articlePost);

    @DELETE("v1/api/articles/{id}")
    Call<Article> deleteArticle(@Path("id") int id);

    @PUT("v1/api/articles/{id}")
    Call<ArticlePost> updateArticle(@Path("id") int id, @Body ArticlePost articlePost);
}
