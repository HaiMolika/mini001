package com.kshrd.mini1.data.remote;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class BasicInterceptorHelper implements Interceptor {

    public final static String credential = "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=";

    public BasicInterceptorHelper() {
    }

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        Request request = chain.request();
        Request.Builder authenticatedRequest = request.newBuilder()
                .addHeader("Authorization", credential)

                .method(request.method(), request.body());
        return chain.proceed(authenticatedRequest.build());
    }
}