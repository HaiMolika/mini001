package com.kshrd.mini1.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArticleResponse {

    private int code;
    private String message;
    @SerializedName("data")
    private List<Article> articles;
    private Pagination pagination;

    public ArticleResponse() {
    }

    public ArticleResponse(int code, String message, List<Article> articles, Pagination pagination) {
        this.code = code;
        this.message = message;
        this.articles = articles;
        this.pagination = pagination;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    @Override
    public String toString() {
        return "ArticleResponse{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", articles=" + articles +
                ", pagination=" + pagination +
                '}';
    }
}
