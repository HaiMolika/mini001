package com.kshrd.mini1.data.models;

public class ArticlePost {

    private String title;
    private String description;
    private String image;

    @Override
    public String toString() {
        return "ArticlePost{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ArticlePost(String title, String description, String image) {
        this.title = title;
        this.description = description;
        this.image = image;
    }

    public ArticlePost() {
    }
}
