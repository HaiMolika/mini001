package com.kshrd.mini1.data.models;

public class Pagination {

    private int page;
    private int limit;
    private int total_count;
    private int total_pages;

    public Pagination() {
        this.page = 1;
        this.limit = 15;
    }

    public Pagination(int page, int limit) {
        this.page = page;
        this.limit = limit;
    }

    public Pagination(int page, int limit, int total_count, int total_pages) {
        this.page = page;
        this.limit = limit;
        this.total_count = total_count;
        this.total_pages = total_pages;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getTotal_count() {
        return total_count;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    @Override
    public String toString() {
        return "Pagination{" +
                "page=" + page +
                ", limit=" + limit +
                ", total_count=" + total_count +
                ", total_pages=" + total_pages +
                '}';
    }
}
