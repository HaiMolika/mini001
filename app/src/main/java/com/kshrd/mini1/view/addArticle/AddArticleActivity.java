package com.kshrd.mini1.view.addArticle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kshrd.mini1.R;
import com.kshrd.mini1.data.models.ArticlePost;
import com.kshrd.mini1.data.models.ImageResponse;
import com.kshrd.mini1.view.addArticle.addArticleViewModel.ArticleViewModel;

import java.io.File;
import java.util.UUID;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddArticleActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_CODE = 100;
    private static final String TAG = "AddArticleActivity tag";

    private ImageView image;
    private EditText title;
    private EditText description;
    private Button btnAdd;
    private ArticleViewModel articleViewModel;
    private MultipartBody.Part body;
    private String image_url;
    private Uri imageUri;
    private Boolean isUpdate;
    private int id;
    private MutableLiveData<ImageResponse> imageResponse;
    private ImageResponse img;

    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_article);

        image = findViewById(R.id.image);
        title = findViewById(R.id.title);
        description = findViewById(R.id.description);
        btnAdd = findViewById(R.id.add);

        requestStoragePermission();

        intent = getIntent();
        isUpdate = intent.getBooleanExtra("isUpdate", false);
        id = intent.getIntExtra("id", 0);

        if(isUpdate){
            String iTitle = intent.getStringExtra("title");
            String iDesc = intent.getStringExtra("description");
            String iImage = intent.getStringExtra("image_url");

            Glide.with(this)
                    .load(iImage)
                    .placeholder(R.drawable.default_image)
                    .dontTransform()
                    .into(image);
            title.setText(iTitle);
            description.setText(iDesc);
            btnAdd.setText(intent.getStringExtra("text"));
            image_url = iImage;
        }

        btnAdd.setOnClickListener(this);
        image.setOnClickListener(this);

        articleViewModel = ViewModelProviders.of(this).get(ArticleViewModel.class);
        articleViewModel.init();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK){
            if(data != null && data.getData() != null){

                Toast.makeText(this, data.getData().toString(), Toast.LENGTH_SHORT).show();
                imageUri = data.getData();

                Glide.with(this)
                        .load(imageUri)
                        .placeholder(R.drawable.default_image)
                        .dontTransform()
                        .into(image);


                File file = new File(getPath(imageUri));
                Log.d( "getPath: " ,"" + file.getAbsolutePath());
                articleViewModel.postImage(file);
                imageResponse = articleViewModel.getImageResponse();
                articleViewModel.getImageResponse().observe(this, new Observer<ImageResponse>() {
                    @Override
                    public void onChanged(ImageResponse imageResponse) {
                        img = imageResponse;
                        image_url = imageResponse.getData();
                        Log.d(TAG, "onChanged: " + imageResponse.getData());
                    }
                });

            }
        }
    }

    String getPath(Uri uri){

        Cursor cursor = this.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();
        cursor = this.getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();
        Log.d( "getPath: " ,"" + path);
        return path;
    }

    void openImageFile(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, REQUEST_CODE);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.image:
                openImageFile();
                break;
            case R.id.add :
                addArticle();
                clearText();
                break;
            default:
                break;
        }
    }

    void addArticle(){

        ArticlePost articlePost = new ArticlePost(title.getText().toString().trim(),
                description.getText().toString().trim(),
                image_url);

        if(isUpdate){
            articleViewModel.updateArticle(id, articlePost);
            articleViewModel.getResponseUpdate().observe(this, new Observer<ArticlePost>() {
                @Override
                public void onChanged(ArticlePost articlePost) {
                    Log.d(TAG, "onChanged: " + articlePost.toString());
                }
            });
        } else {
            if (img.getCode() == 2222){
                articleViewModel.postArticle(articlePost);
                articleViewModel.getArticlePost().observe(this, new Observer<ArticlePost>() {
                    @Override
                    public void onChanged(ArticlePost articlePost) {
                        Log.d(TAG, "onChanged: " + articlePost);
                    }
                });
                Toast.makeText(getApplication(), "Successfully Article Added", Toast.LENGTH_SHORT).show();
            }
        }

    }

    void clearText(){
        image.setImageResource(R.drawable.default_image);
        title.setText("");
        description.setText("");
    }

    //Requesting permission
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
        }
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == 1) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }

}