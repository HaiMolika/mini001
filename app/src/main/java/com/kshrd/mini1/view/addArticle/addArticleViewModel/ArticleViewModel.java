package com.kshrd.mini1.view.addArticle.addArticleViewModel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.kshrd.mini1.data.models.ArticlePost;
import com.kshrd.mini1.data.models.ImageResponse;
import com.kshrd.mini1.repositories.ArticleRepository;

import java.io.File;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;

public class ArticleViewModel extends ViewModel {

    private ArticleRepository repository;
    private MutableLiveData<ArticlePost> articlePost;
    private MutableLiveData<ArticlePost> responseUpdate;
    private MutableLiveData<ImageResponse> imageResponse;

    public void init(){
        repository = new ArticleRepository();
    }

    public void postImage(File file){
        imageResponse = repository.postImage(file);
    }

    public void postArticle(ArticlePost post){
        articlePost = repository.postArticle(post);
    }

    public void updateArticle(int id, ArticlePost articlePost){
        responseUpdate = repository.updateArticle(id, articlePost);
    }

    public MutableLiveData<ArticlePost> getArticlePost() {
        return articlePost;
    }

    public MutableLiveData<ImageResponse> getImageResponse() {
        return imageResponse;
    }

    public MutableLiveData<ArticlePost> getResponseUpdate(){
        return responseUpdate;
    }

}
