package com.kshrd.mini1.view.showArticle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kshrd.mini1.R;
import com.kshrd.mini1.data.models.Article;

public class ViewArticleActivity extends AppCompatActivity {

    private TextView title, date, description;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_article);

        title = findViewById(R.id.title);
        date = findViewById(R.id.date);
        description = findViewById(R.id.description);
        image = findViewById(R.id.image);

        Intent intent = getIntent();
        String mdate = intent.getStringExtra("date");
        String newDate = mdate.substring(0, 4) + "/" + mdate.substring(4,6) + "/" + mdate.substring(6, 8);
        Glide.with(this)
                .load(intent.getStringExtra("image"))
                .placeholder(R.drawable.default_image)
                .dontTransform()
                .into(image);
        if(intent.getStringExtra("title") != null)
            title.setText(intent.getStringExtra("title"));
        if(intent.getStringExtra("date") != null)
            date.setText(newDate);
        if(intent.getStringExtra("description") != null)
            description.setText(intent.getStringExtra("description"));

    }
}