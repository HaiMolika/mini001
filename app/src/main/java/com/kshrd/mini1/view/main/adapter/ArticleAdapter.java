package com.kshrd.mini1.view.main.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.kshrd.mini1.R;
import com.kshrd.mini1.data.models.Article;
import com.kshrd.mini1.view.addArticle.AddArticleActivity;
import com.kshrd.mini1.view.showArticle.ViewArticleActivity;

import java.util.List;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ArticleViewHolder>{

    private Context context;
    private List<Article> articles;
    private MenuItemEvent menuItemEvent;
    public static Article article;
    public static int position;

    public ArticleAdapter(Context context, List<Article> articles, MenuItemEvent menuItemEvent) {
        this.context = context;
        this.articles = articles;
        this.menuItemEvent = menuItemEvent;
    }

    public void setArticles(List<Article> articles){
        Log.d("tag" ,"setArticles: " + articles.size());
        this.articles = articles;
    }

    public void appendArticles(List<Article> articles){
        this.articles.addAll(articles);
        Log.d("tag" ,"appendArticles: " + this.articles.size());
    }
    @NonNull
    @Override
    public ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.article_item, null);
        ArticleViewHolder viewHolder = new ArticleViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleViewHolder holder, int position) {
        Article article = articles.get(position);
        if(article.getTitle() != null){
            holder.tvTitle.setText(article.getTitle());
        }
        if(articles.get(position).getImage_url() != null){
            Glide.with(context)
                    .load(article.getImage_url())
                    .placeholder(R.drawable.default_image)
                    .dontTransform()
                    .into(holder.image);
        }
    }

    @Override
    public int getItemCount() {
        if(articles != null){
            return articles.size();
        }
        return 0;
    }

    public class ArticleViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;
        private ImageView image, setting;

        public ArticleViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.article_title);
            image = itemView.findViewById(R.id.article_image);
            setting = itemView.findViewById(R.id.setting);
            setting.setVisibility(View.GONE);

            setting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popupMenu = new PopupMenu(v.getContext(), v);
                    popupMenu.inflate(R.menu.item_menu);
                    popupMenu.show();
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()){
                                case R.id.edit:
                                    menuItemEvent.menuItemonUpdate(article, position);
                                    setting.setVisibility(View.GONE);
                                    return true;
                                case R.id.delete:
                                    menuItemEvent.menuItemOnDelete(article, position);
                                    setting.setVisibility(View.GONE);
                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                }

            });
            itemView.setClickable(true);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    article = articles.get(getAdapterPosition());
                    position = getAdapterPosition();
                    setting.setVisibility(View.VISIBLE);
                    return true;
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    article = articles.get(getAdapterPosition());
                    Intent intent = new Intent(v.getContext(), ViewArticleActivity.class);
                    intent.putExtra("title", article.getTitle());
                    intent.putExtra("description", article.getDescription());
                    intent.putExtra("date", article.getCreated_date());
                    intent.putExtra("image", article.getImage_url());
                    v.getContext().startActivity(intent);
                }
            });

        }
    }
}
