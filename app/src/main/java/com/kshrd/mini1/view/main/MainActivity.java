package com.kshrd.mini1.view.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.kshrd.mini1.R;
import com.kshrd.mini1.data.models.Article;
import com.kshrd.mini1.data.models.ArticleResponse;
import com.kshrd.mini1.view.addArticle.AddArticleActivity;
import com.kshrd.mini1.view.main.adapter.ArticleAdapter;
import com.kshrd.mini1.view.main.adapter.MenuItemEvent;
import com.kshrd.mini1.view.main.mainViewModel.MainActivitViewModel;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.kshrd.mini1.view.main.PaginationMaker.PAGE_START;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, MenuItemEvent {

    private static final String TAG = "MainActivity tag";
    private Bundle s;

    private LinearLayoutManager layoutManager;
    private ArticleResponse articles;
    private RecyclerView recyclerView;
    private ArticleAdapter articleAdapter;
    private MainActivitViewModel viewModel;
    private SwipeRefreshLayout swipeRefresh;
    private FloatingActionButton btnAdd;
    private ProgressBar progressBar;

    private boolean isReload = false;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int currentPage = PAGE_START;

    int count = 0;
    Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        s = savedInstanceState;

        btnAdd = findViewById(R.id.btn_add);
        recyclerView = findViewById(R.id.article_list);
        swipeRefresh = findViewById(R.id.swipe_refresh);
        progressBar = findViewById(R.id.progress);
        progressBar.setVisibility(View.GONE);

        viewModel = ViewModelProviders.of(this).get(MainActivitViewModel.class);
        viewModel.init();

        getArticleResponse();
        initSwipeRefresh();

        btnAdd.setOnClickListener(this);

    }

    private void setupProgress(){
        timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                count ++;
                progressBar.setProgress(count);
                if (count == 100){

                    timer.cancel();
                }
            }
        };
       timer.schedule(timerTask, 0, 100);
    }

    private void initRecyclerView(){
        if(articleAdapter == null){
            articleAdapter = new ArticleAdapter(this, articles.getArticles(), this);
            layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(articleAdapter);
        } else if(isReload){
            articleAdapter.setArticles(articles.getArticles());
            articleAdapter.notifyDataSetChanged();
            isReload = false;
        }
        else if (!isReload){
            articleAdapter.appendArticles(articles.getArticles());
            articleAdapter.notifyDataSetChanged();
        }
        recyclerView.setOnScrollListener(new PaginationMaker(layoutManager) {
            @Override
            protected void loadMoreItem() {
                isLoading= true;
                currentPage ++ ;
                viewModel.getPagination().setPage(currentPage);

                getArticleResponse();
            }

            @Override
            protected boolean isLastPage() {
                isLastPage = currentPage == viewModel.getPagination().getTotal_pages();
                return isLastPage;
            }

            @Override
            protected boolean isLoading() {
                return isLoading;
            }

            @Override
            public void fabShowing(Boolean show) {
                if (show){
                    btnAdd.setVisibility(View.VISIBLE);
                }
                else {
                    btnAdd.setVisibility(View.GONE);
                }
            }
        });
    }

    private void initSwipeRefresh(){
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.d(TAG, "onRefresh: ");
                isReload = true;
                currentPage = PAGE_START;
                isLastPage = false;
                isLoading = false;
                Log.d(TAG, "current page : " + currentPage);
                viewModel.getPagination().setPage(currentPage);
                getArticleResponse();
            }

        });
    }

    private void getArticleResponse(){
        viewModel.setLiveData();
        viewModel.getLiveData().observe(this, new Observer<ArticleResponse>() {
            @Override
            public void onChanged(ArticleResponse articleResponse) {
                Log.d(TAG, "onChanged: " + viewModel.getPagination().getLimit()+ " ,"+ articleResponse.getArticles().size());
                articles = null;
                articles = articleResponse;
                initRecyclerView();
                if (swipeRefresh.isRefreshing()){
                    swipeRefresh.setRefreshing(false);
                }
            }
        });

        progressBar.setVisibility(View.GONE);
        isLoading = false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        recyclerView.smoothScrollToPosition(0);
        return true;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, AddArticleActivity.class);
        intent.putExtra("isUpdate", false);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void menuItemonUpdate(Article article, int Postion) {
        Toast.makeText(this, "here", Toast.LENGTH_SHORT).show();
        Log.d("onCreateContextMenu: ", article.toString());
        Intent intent = new Intent(this, AddArticleActivity.class);

        intent.putExtra("isUpdate", true);
        intent.putExtra("text", "Update");
        intent.putExtra("id", article.getId());
        intent.putExtra("title", article.getTitle());
        intent.putExtra("image_url", article.getImage_url());
        intent.putExtra("description", article.getDescription());

        startActivity(intent);
    }

    @Override
    public void menuItemOnDelete(Article article, int position) {
        final Boolean[] isDelete = {true};
        articles.getArticles().remove(article);
        articleAdapter.notifyItemRemoved(position);
        Snackbar.make(recyclerView, article.getId() + " is deleting", Snackbar.LENGTH_LONG)
                .addCallback(new Snackbar.Callback(){
                    @Override
                    public void onDismissed(Snackbar transientBottomBar, int event) {
                        super.onDismissed(transientBottomBar, event);
                        viewModel.deleteArticle(article.getId());
                    }
                })
                .setAction("Undo", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        articles.getArticles().add(position, article);
                        articleAdapter.notifyItemInserted(position);
                        isDelete[0] = false;
                        Log.d(TAG, "onClick: show");
                    }
                }).setDuration(2000).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.onCreate(s);
    }
}