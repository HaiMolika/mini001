package com.kshrd.mini1.view.main;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public abstract class PaginationMaker extends RecyclerView.OnScrollListener {

    private LinearLayoutManager layoutManager;
    public static final int PAGE_START = 1;
    private static final int PAGE_SIZE = 15;
    private Boolean FETCH_NEW_DATA = false;

    public PaginationMaker(LinearLayoutManager layoutManager){
        this.layoutManager = layoutManager;
    }

    @Override
    public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int totalItem = layoutManager.getItemCount();
        int visibleItem = layoutManager.getChildCount();
        int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();

            if(!isLoading() && !isLastPage()){
                if((visibleItem + firstVisibleItem)>= totalItem
                        && firstVisibleItem>=0
                        && totalItem>=PAGE_SIZE){
                        loadMoreItem();
                }
            FETCH_NEW_DATA = false;
        }

        if (dy>0){
            fabShowing(false);
        }
        else if(dy<=0){
            fabShowing(true);
        }

    }

    protected abstract void loadMoreItem();
    protected abstract boolean isLastPage();
    protected abstract boolean isLoading();
    public abstract void fabShowing(Boolean show);
}
