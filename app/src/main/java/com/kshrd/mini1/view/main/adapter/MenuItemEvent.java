package com.kshrd.mini1.view.main.adapter;

import android.widget.PopupMenu;

import com.kshrd.mini1.data.models.Article;

public interface MenuItemEvent{
    public void menuItemonUpdate(Article article, int Postion);
    public void menuItemOnDelete(Article article, int Postion);
}
