package com.kshrd.mini1.view.main.mainViewModel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.kshrd.mini1.data.models.ArticlePost;
import com.kshrd.mini1.data.models.ArticleResponse;
import com.kshrd.mini1.data.models.Pagination;
import com.kshrd.mini1.repositories.ArticleRepository;

public class MainActivitViewModel extends ViewModel {

    private MutableLiveData<ArticleResponse> liveData;
    private Pagination pagination;
    private ArticleRepository repository;

    public void init(){
        repository = new ArticleRepository();
        pagination = new Pagination();
    }

    public void setLiveData(){
        liveData = new MutableLiveData<>();
        liveData = repository.getArticleByPagination(pagination);
    }

    public Pagination getPagination() {
        return pagination;
    }

    public MutableLiveData<ArticleResponse> getLiveData(){
        return liveData;
    }

    public void deleteArticle(int id){
        Log.d( "delete", "ViewModel" );
        repository.deleteArticle(id);
    }

}
